<?php
//Realizar una expresión regular que detecte emails correctos.
$check_correo='cisoriano99@gmail.com';
$result_correo= preg_match('/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/',$check_correo);
echo $result_correo; //Si sale 1 la expresion puede leer cualquier correo

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$check_curp='SOBC990614HDFRNR09';//Si se remplaza por el curp anterior dara 0 lo cual no es valido
$result_curp= preg_match('/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/',$check_curp);
echo $result_curp; //Si sale 1 la expresion puede leer cualquier curp


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$check_palabra='qazwsxedcrfvtgbyhnujmikolppolikujmyhntgbrfvedcws';
$result_palabra=preg_match('/^[a-zA-Z]*$/',$check_palabra);
echo $result_palabra; //Si sale 1 la expresion puede leer cualquier palabra

//Crea una funcion para escapar los simbolos especiales.
$cadena = 'hola "estoy entre comillas dobles"';
echo htmlspecialchars($cadena);
//elimina caracteres especiales

//Crear una expresion regular para detectar números decimales.
$check_numeroDeci=-3431.336;
$result_numeroDeci=preg_match('/^(\d|-)?(\d|,)*\.?\d*$/',$check_numeroDeci);
echo $result_numeroDeci; //Si sale 1 la expresion hay un numero decimal

?>
