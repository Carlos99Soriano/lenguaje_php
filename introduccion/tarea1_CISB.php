<html> 
	<head> 
		<title>Formas</title> 
	</head> 
	<body> 
        <div>
        <center>
            <h1>Piramide y Rombo </h1>
            <p>Esta pagina tiene como finalidad la construccion de 2 figuras una piramide y un rombo</p>
            <p>Para ello el usuario debe de ingresar el numero de filas con las que podra ser construida</p>
            <p>Por favor ingrese un numero de filas del 1 al 30</p> <br>
            <form method="post" >
                Numero de filas: <input type="text" name="filas"></p>
                <p><input type="submit" value="Enviar" ></p>
            </form>
        </center> 
        </div>
        <div >
            <?php
                $numero = $_POST["filas"]; 
                if ($numero== "") {
                    echo "<p align=\"center\">No ha escrito ningún numero</p>";
                } else {
                    if($numero<= 30){
                        echo "<p align=\"center\">Ha escrito $numero filas </p>";
                        // Piramide 
                        for($i=1;$i<=$numero;$i++){
                            for($j=1;$j<=$numero-$i;$j++){
                                echo "&nbsp&nbsp";
                            }
                                for($j=1;$j<=2*$i-1;$j++){
                                    echo ("*");}
                                echo "<br />";
                        }
                        echo "<br />";
                        echo "<br />";
                        echo "<br />";
                        //Rombo
                        for($x=1;$numero-1>=$x;$x++){
                            for($y=$numero;$y>=$x;$y--){
                                echo "&nbsp&nbsp";
                            }
                            for ($z=1;2*$x-1>=$z;$z++){
                                echo ("*");
                            }
                            echo "<br/>";
                        }
                        for($x=1;$numero>=$x;$x++){
                            for($y=1;$y<=$x;$y++){
                                echo "&nbsp&nbsp";
                            }
                            for($z=2*$numero-1;2*$x-1<=$z;$z--){
                                echo "*";
                            }
                            echo "<br/>";
                        }
                        echo "<br/>";
                    }else{
                        echo '<script >alert("Paso el limite de 30 filas intente con un numero más pequeño");</script>';
                    }
                }     
            ?>
        </div>
	</body>
</html> 